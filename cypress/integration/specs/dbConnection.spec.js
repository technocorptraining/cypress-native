describe('test mysql db', () => {

    it.skip('form submission and verify db results', () => {

        // UI form submission
        // done submission with success message 

        // verify DB results
        cy.task('queryDB', 'SELECT * from submissions_table WHERE email=testemail@gmail.com').then((results) => {
            expect(results).to.have.lengthOf(1);
            expect(results[0].email).to.eq('testemail@gmail.com');
            expect(results[0].name).to.eq('Anil kumar');
            expect(results[0].phone).to.eq('919123232323');
            expect(results[0].country).to.eq('India');

        })
    })
})