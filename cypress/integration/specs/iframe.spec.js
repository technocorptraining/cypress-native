/// <reference types="cypress-iframe" />

describe('iframe handling', () => {

    it.only('check iframe text input', () => {
        cy.visit('https://the-internet.herokuapp.com/iframe');
        cy.getIframeBody()
            .find('p').clear();
        cy.getIframeBody()
            .find('p').type('This is an iframe').should('have.text', 'This is an iframe')
    });

    it('check iframe text input using cypress-iframe module', () => {
        cy.visit('https://the-internet.herokuapp.com/iframe');
        cy.frameLoaded('#mce_0_ifr')
        cy.iframe().find('p').clear()
        cy.iframe().find('p').type('This is an iframe').should('have.text', 'This is not an iframe')
    });

})