
describe('handling jquery elements', () => {

    it('inbuilt cypress jquery', () => {
        let $li = Cypress.$(".class li:first");
        cy.wrap($li)
            .click()
    })

    it('inbuilt cypress jquery checking element active or not', () => {
        let $li = Cypress.$(".class li:first");
        cy.wrap($li)
            .should('not.have.class', 'active')
            .click()
            .should('have.class', 'active')
    })

    it('to handle hidden element', () => {
        cy.get(loc).invoke('show').click();
    })

    it('to getText using jquery', () => {
        cy.get(loc).invoke('text').then((text) => {
            expect(text).to.eq('Automation')
        })
    })

    // in cypress another method - cy.$$()
    it('usage of cy.$$', () => {
        cy.$$(loc).each([1, 2, 3], (index, value) => {
            expect(index).to.eq(value)
        })
    })
})