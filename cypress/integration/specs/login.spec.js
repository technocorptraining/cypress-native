import LoginPage from '../pageObjects/login.po'
var data = require('../../fixtures/testdata.json')

describe('loginPage', () => {

    it('check valid login', () => {
        const login = new LoginPage()
        login.openBrowser()
        login.wait();
        login.enterUsername(data.username)
        login.enterPassword(data.password)
        login.clickLoginbtn()
        login.verifyDashboardtxt()
    })
})