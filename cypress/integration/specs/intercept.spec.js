describe('using intercept ', () => {

    beforeEach(() => {
        cy.intercept('GET', '**/tags', { fixture: 'tags.json' });
        cy.intercept('GET', '**/articles*', { fixture: 'articles.json' })
        cy.visit('https://angular.realworld.io/')
        cy.wait(2000)
    })

    it('check tags on UI', () => {
        cy.get('.tag-list a').should('contain', 'cypress')
            .and('contain', 'selenium')
    })

    it('check articles on UI', () => {
        cy.get('.preview-link h1').should('contain', 'This is a cypress intercept blog')
    })
})