
import LoginPage from '../pageObjects/login.po';
import LoginPage2 from '../pageObjects/login.po.method2';
const login = new LoginPage2();

require('cypress-iframe');

Cypress.Commands.add('login', (data) => {
    cy.visit('/');
    login.getUsernameLoc().clear().type(data.username)
    login.enterPassword().clear().type(data.password)
    login.Loginbtn().click();
})