/// <reference types="Cypress" />
var data = require('../../fixtures/testdata.json')

class LoginPage2 {

    openBrowser() {
        cy.visit(Cypress.env(data.env))
    }

    wait() {
        cy.wait(3000)
    }

    getUsernameLoc() {
        return cy.get('#txtUsername')
    }

    enterPassword() {
        return cy.get('#txtPassword')
    }

    Loginbtn() {
        return cy.get('#btnLogin')
    }

    verifyDashboardtxt() {
        cy.contains(data.dashboardTxt).should('be.visible', { timeout: 20000 })
    }
}

export default LoginPage2