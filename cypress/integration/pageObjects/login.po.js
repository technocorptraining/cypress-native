/// <reference types="Cypress" />
var data = require('../../fixtures/testdata.json')

class LoginPage {

    openBrowser() {
        cy.visit(Cypress.env(data.env))
    }

    wait() {
        cy.wait(3000)
    }

    enterUsername(data) {
        const field = cy.get('#txtUsername')
        field.clear().type(data)
        return this
    }

    enterPassword(data) {
        const field = cy.get('#txtPassword')
        field.clear().type(data)
        return this
    }

    clickLoginbtn() {
        const field = cy.get('#btnLogin')
        field.click().wait(2000)
    }

    verifyDashboardtxt() {
        cy.contains(data.dashboardTxt).should('be.visible', {timeout: 20000})
    }
}

export default LoginPage