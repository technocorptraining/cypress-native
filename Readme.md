# git install
1. Install from here: https://git-scm.com/
2. git init   // initialize in the project
3. git status
4. git add .   // to add all the files
5. git add filePath  // to add specific file
6. git commit -m "updated login specs"
7. git push // to push local changes to remote
8. git pull  // to fetch remote changes to local


# dev branches 
1. master 
2. sivagami_dev
3. avinash_dev
4. lalith_dev
5. 

or 

# feature branch
1. login_feature
2. admin_feature
3. payment_feature


# Jenkins - Freestyle project
1. Install - Nodejs plugin from Jenkins
2. Go to Global tool Config > set global nodejs > set dependencies
2. Create a Job
3. Create config


# docker images
https://github.com/cypress-io/cypress-docker-images


